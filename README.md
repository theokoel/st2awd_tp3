# Théo Koël
## Question 1
***That is the main difference between local installation and global installation of packages with npm? What kind of packages do you generally install locally? What kind is generally installed globally?***

Generaly we install globaly the CLI of the applications or frameworks.

## Question 2:
***Webpack is internally used by the Vue CLI. Why is it required to deal with both multiple JavaScript files and special extensions like .vue?***

We develope with multiple js file and dile .vue because il's more ease and undertanble for the developpement and .vue file is file was compiled with VueJS framework, so they isn't execute without compilcation before.

## Question 3
***What is the role of babel and how browserslist may configure its output?***

Babel is used for adapte the code to the old browser and he look if the code is correcly write for his translation.

## Question 4
***What is eslint and which set of rules are currently applied? The eslint configuration may be defined in a eslint.config.js or in package.json depending on the setup.***

Eslint is a tool for looking le quality of code and look if he didn't have ambigious terms who can create bugs. And by default, vue CLI didn't create a eslint.config.js file for his configuration, so he is configurable in the package.json.

## Question 5
***What is the difference between scoped and non-scoped CSS?***

Scoped is defint the CSS was write in a component is only apply to the component. So he assured the style of a components change the style of a other under the will.

## Question 6
***How behaves non-prop attributes (aka. HTML attributes) passed down to a component, when its template has a single root element? Tips: it is well documented by vue, but you can also try it youself by passing the style attribute with a straight visual effect.***

When a component returns a single root node, non-prop attributes will automatically be added to the root node's attributes.

## Question 7
***Analyse how works the AsyncButton. When is the parent @click event called? How the child component is aware of the returned Promise by the parent onClick handler? Why is the await necessary? Etc.***

The asyncButton is a child of a page and with the inject data of VueJS the asyncButton have access of DOM of his parent. But the parent function need to be in async/await for access by the child, because without the function will be execute in another thread and was plite of the children injection.

## Question 8:
***Which bug is introduced if inheritAttrs: false is missing or set to true in AsyncButton. Why?***

By default, parent scope attribute bindings that are not recognized as props will "fallthrough". This means that when we have a single-root component, these bindings will be applied to the root element of the child component as normal HTML attributes. When authoring a component that wraps a target element or another component, this may not always be the desired behavior. By setting inheritAttrs to false, this default behavior can be disabled. The attributes are available via the $attrs instance property and can be explicitly bound to a non-root element using v-bind.
(source VueJS docs)
